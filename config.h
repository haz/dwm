/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const char *fonts[]          = { "monospace:size=10", "IPAGothic:size=10", "Symbola:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_black[]       = "#000000";
static const char col_white[]       = "#ffffff";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_black, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray2,  col_gray3 },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ NULL,       "float",    NULL,       0,            1,           -1 },
	{ "Dragon-drop", NULL,    NULL,      ~0,            1,           -1 },
	{ NULL,       NULL,   "Event Tester", 0,            1,           -1 },
	{ "Minecraft 1.8.9",   NULL, NULL,    0,            1,           -1 },
	{ "Minecraft 1.12.2",  NULL, NULL,    0,            1,           -1 },
	{ "Minecraft* 1.20.4", NULL, NULL,    0,            1,           -1 },
	{ NULL,   "Godot_Engine", NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

/* custom layout declarations */
static void deck(Monitor *m);

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[D]",      deck },
};

/* custom functions declarations */
static unsigned int adjtag(const Arg *arg);
static void viewadj(const Arg *arg);
static void tagadj(const Arg *arg);
static void toggletagadj(const Arg *arg);

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

#define EXEC(...) { .v = (const char*[]){ __VA_ARGS__, NULL } }

#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_t,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ControlMask|ShiftMask, XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ControlMask|ShiftMask, XK_u,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask|ShiftMask, XK_h,      setmfact,       {.f = -0.025} },
	{ MODKEY|ControlMask|ShiftMask, XK_l,      setmfact,       {.f = +0.025} },
	{ MODKEY,                       XK_z,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	/* { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } }, */
	/* { MODKEY,                       XK_period, focusmon,       {.i = +1 } }, */
	/* { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } }, */
	/* { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } }, */
	{ MODKEY,                       XK_comma,  viewadj,        {.i = -1 } },
	{ MODKEY,                       XK_period, viewadj,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagadj,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagadj,         {.i = +1 } },
	{ MODKEY|ControlMask|ShiftMask, XK_comma,  toggletagadj,   {.i = -1 } },
	{ MODKEY|ControlMask|ShiftMask, XK_period, toggletagadj,   {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {0} },
	{ 0, XF86XK_AudioMute,                     spawn,          EXEC("vol", "mute") },
	{ 0, XF86XK_AudioRaiseVolume,              spawn,          EXEC("vol", "up") },
	{ 0, XF86XK_AudioLowerVolume,              spawn,          EXEC("vol", "down") },
	{ MODKEY,                       XK_i,      spawn,          EXEC("vol", "up") },
	{ MODKEY,                       XK_u,      spawn,          EXEC("vol", "down") },
	{ MODKEY,                       XK_m,      spawn,          EXEC("vol", "mute") },
	{ MODKEY|ShiftMask,             XK_i,      spawn,          EXEC("bri", "up") },
	{ MODKEY|ShiftMask,             XK_u,      spawn,          EXEC("bri", "down") },
	/* { MODKEY|ControlMask,           XK_space,  spawn,          EXEC("togglepad") }, */
	{ MODKEY, XF86XK_PowerOff,                 spawn,          EXEC("sysact") },
	{ MODKEY,                       XK_Delete, spawn,          EXEC("sysact") },
	{ 0, XF86XK_PowerOff,                      spawn,          EXEC("xset", "dpms", "force", "off") },
	{ MODKEY,                       XK_Escape, spawn,          EXEC("slock") },
	{ MODKEY,                       XK_a,      spawn,          EXEC("menu_a") },
	{ MODKEY,                       XK_x,      spawn,          EXEC("menu_x") },
	{ MODKEY,                       XK_semicolon, spawn,       EXEC("menu_semi") },
	{ MODKEY,                       XK_s,      spawn,          EXEC("ddic") },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          EXEC("web") },
	{ MODKEY|ShiftMask,             XK_w,      view,           {.ui = 1 << 8 } },
	{ MODKEY,                       XK_g,      spawn,          EXEC("dgen") },
	{ MODKEY,                       XK_apostrophe, spawn,      EXEC("dsnip") },
	{ MODKEY|ShiftMask,             XK_apostrophe, spawn,      EXEC("duni") },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          EXEC("mpvc", "toggle") },
	{ MODKEY|ShiftMask,             XK_h,      spawn,          EXEC("mpvc", "backward") },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          EXEC("mpvc", "forward") },
	{ MODKEY,                       XK_Pause,  spawn,          EXEC("mpvc", "stop") },
	{ 0,                            XF86XK_AudioPlay, spawn,   EXEC("mpvc", "toggle") },
	{ 0,                            XF86XK_AudioStop, spawn,   EXEC("mpvc", "stop") },
	{ 0,                            XF86XK_AudioNext, spawn,   EXEC("mpvc", "next") },
	{ MODKEY,                       XK_n,      spawn,          EXEC("mpvc", "next") },
	{ 0,                            XF86XK_AudioPrev, spawn,   EXEC("mpvc", "prev") },
	{ MODKEY,                       XF86XK_Forward,   spawn,   EXEC("mpvc", "forward") },
	{ MODKEY,                       XF86XK_Back,      spawn,   EXEC("mpvc", "backward") },
	{ 0,                            XF86XK_Display,   spawn,   EXEC("ddisplaycon") },
	{ MODKEY|ShiftMask,             XK_Pause,  spawn,          EXEC("W") },
	{ MODKEY,                       XK_9,      spawn,          EXEC("W", "c") },
	{ MODKEY,                       XK_slash,  spawn,          EXEC("dwls") },
	{ MODKEY,                       XK_backslash, spawn,       EXEC("scratchpad") },
	{ MODKEY,                       XK_F1,     spawn,          EXEC("univ-menu") },
	{ MODKEY,                       XK_F2,     spawn,          EXEC("mpvc", "playlist") },
	{ MODKEY,                       XK_F3,     spawn,          EXEC("dmpvc") },
	{ 0,                            XK_Print,  spawn,          EXEC("sshot") },
	{ MODKEY,                       XK_Print,  spawn,          EXEC("sshot", "-s") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	/* { ClkWinTitle,          0,              Button1,        spawn,          EXEC("togglepad") }, */
};

/* custom functions */
unsigned int
adjtag(const Arg *arg)
{
	unsigned int seltag = selmon->tagset[selmon->seltags];
	if (arg->i > 0)
		return seltag == (1 << (LENGTH(tags) - 1)) ? 1 : seltag << 1;
	else
		return seltag == 1 ? (1 << (LENGTH(tags) - 1)) : seltag >> 1;
}

void
viewadj(const Arg *arg)
{
	view(&(const Arg){.ui = adjtag(arg)});
}

void
tagadj(const Arg *arg)
{
	if (selmon->sel == NULL)
		return;
	tag(&(const Arg){.ui = adjtag(arg)});
}

void
toggletagadj(const Arg *arg)
{
	if (selmon->sel == NULL)
		return;
	toggletag(&(const Arg){.ui = adjtag(arg)});

}

void
deck(Monitor *m) {
	unsigned int i, n, h, mw, my;
	Client *c;

	for(n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if(n == 0)
		return;

	if(n > m->nmaster) {
		mw = m->nmaster ? m->ww * m->mfact : 0;
		snprintf(m->ltsymbol, sizeof m->ltsymbol, "[%d]", n - m->nmaster);
	}
	else
		mw = m->ww;
	for(i = my = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
		if(i < m->nmaster) {
			h = (m->wh - my) / (MIN(n, m->nmaster) - i);
			resize(c, m->wx, m->wy + my, mw - (2*c->bw), h - (2*c->bw), False);
			my += HEIGHT(c);
		}
		else
			resize(c, m->wx + mw, m->wy, m->ww - mw - (2*c->bw), m->wh - (2*c->bw), False);
}
